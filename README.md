# petStoreV2
Projeto de Automação de API construído com: Java, Cucumber, BDD, Gherkin, Junit, Rest-Assured.

O projeto foi feito com a arquitetura do page object, utilizando o padrão de design patter.
Atendendo os principais metodos do swagger PetStore.
Foi criado também um db com dois arquivos json para alimentar a massa de dados para a automação.
E por fim, foi incrementado também um plugin para fazer/gerar o relatorio com o resultado de todos os testes.

25/01/2023 - implementado uma pipeline para rodar os testes integrados dessa api no gitLab.

Uma imagem docker foi criada para fazer os procedimentos de configuração do java e outros pacotes de instalação, como: mvn, npm, node

A pipeline para rodar os testes foram construidas com três stages:

Build: é executado o script de testes com o comando do mvn e gerando um relatorio com tempo de expiração de 2 dias
Lint: é executado os scripts de testes todos os dias em um periodo X
Test: é executado somente quando tem um merge request

Automação realizada com base no Swagger:
https://petstore.swagger.io/#/
